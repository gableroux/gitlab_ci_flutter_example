# gitlab_ci_flutter_example

[![pipeline status](https://gitlab.com/gableroux/gitlab_ci_flutter_example/badges/master/pipeline.svg)](https://gitlab.com/gableroux/gitlab_ci_flutter_example/commits/master)
[![coverage report](https://gitlab.com/gableroux/gitlab_ci_flutter_example/badges/master/coverage.svg)](https://gitlab.com/gableroux/gitlab_ci_flutter_example/commits/master)

This project uses [gableroux/docker-flutter](https://github.com/gableroux/docker-flutter) to test and build a flutter application with gitlab-ci.

Go ahead and see [`.gitlab-ci.yml`](.gitlab-ci.yml)

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.io/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.io/docs/cookbook)

For help getting started with Flutter, view our 
[online documentation](https://flutter.io/docs), which offers tutorials, 
samples, guidance on mobile development, and a full API reference.

## License

[MIT](LICENSE.md) © [Gabriel Le Breton](https://gableroux.com)

